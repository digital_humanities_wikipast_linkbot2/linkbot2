# Description
Media wiki Bot running on wikipast.epfl.ch 
* Adds reciprocal link between pages if missing (rework of PageUpdaterBot)
* Split long pages into subpages

Projet description may be found on http://wikipast.epfl.ch/wiki/LinkBot2

The part of the bot that completes the link in all the pages of the wiki need to have access to a dump version of the wikipast site to work correctly. 
This part was only executed on 10 entries to test that it actually works.
We removed the entries created by it just after so that when someone want to launch it on all the site it won't create duplicate if he uses the same dump file as we used.

License for Logo: Font in use CooperHewitt-Semibold designed by Chester Jenkins and licensed under Open Font License. Icon Designed by Alfredo @ IconsAlfredo.com
